# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a complete pagkage for ARM One Integration with all PHP platform.
* Version 1.0

### How do I get set up? ###

* Summary of set up
	- Clone Is Repository
	- Run Composer Install
	- vendor/bin/phpunit tests/ARMOneTest
* Configuration
	- Install Composer
* Dependencies
* Database configuration
	- None
* How to run tests
	- change directory to the project directory
	- run vendor/bin/phpunit tests/ARMOneTest

### Contribution guidelines ###

* Writing tests
test cases can found in tests/ARMOneTest.php the following test cases exists for ARM One.
	- Register Test Case
	- Login Test Case
	- Logout Test Case
	- ValidateARMOneCookie Test Case
	- ChangePassword Test Case
	- ResetPassword Test Case
* Code review
	- Code Review by Omeonu Nnadozie
* Other guidelines

### Who do I talk to? ###

* Omeonu Nnadozie
* ARM IT Team