<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NnadozieOmeonu\ARMOne\Contract;

use NnadozieOmeonu\ARMOne\Beans\Customer;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
interface IdentityManagementRepository
{

    /**
     *
     * @param Customer $customer
     */
    public function register(array $customer);

    /**
     *
     * @param Customer $customer
     * @param string $redirecturl
     */
    public function login(Customer $customer, string $redirecturl);

    /**
     *
     * @param type $token
     */
    public function logout($token);

    public function validateARMOneCookie($token);

    /**
     *
     * @param string $userId
     * @param string $oldPassword
     * @param string $newPassword
     */
    public function changePassword(string $userId, string $oldPassword, string $newPassword);

    /**
     *
     * @param Customer $customer
     */
    public function resetPassword(Customer $customer);
}
