<?php
namespace NnadozieOmeonu\ARMOne\Contract;


use GuzzleHttp\Exception\ClientException;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
interface Requestable
{
    /**
     * Makes a request to an external API
     *
     * @param  string $method
     * @param  string $endpoint
     * @param  array $data
     * @return array
     *
     * @throws ClientException
     */
    public function handle($method, $endpoint, $data);
}
