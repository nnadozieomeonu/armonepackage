<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NnadozieOmeonu\ARMOne\Contract\Impl;

use NnadozieOmeonu\ARMOne\Contract\IdentityManagementRepository;
use NnadozieOmeonu\ARMOne\Beans\Customer;
use NnadozieOmeonu\ARMOne\Services\ARMOneAuthenticationService as ARMOne;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
class IdentityManagement implements IdentityManagementRepository
{

    /**
     *
     * @var type
     */
    private $armOne;

    public function __construct()
    {
        $this->armOne = new ARMOne();
    }

    /**
     *
     * @param string $email
     * @param string $oldPassword
     * @param string $newPassword
     * @return type
     */
    public function changePassword(string $email, string $oldPassword, string $newPassword)
    {
        return $this->armOne->changePassword([
                    "Membershipkey" => $email,
                    "OldPassword" => $oldPassword,
                    "NewPassword" => $newPassword,
                    "IsReset" => false,
                    "Channel" => "ARM_PAYDAY"
        ]);
    }

    /**
     *
     * @param Customer $customer
     * @param string $redirecturl
     * @return type
     */
    public function login(Customer $customer, string $redirecturl)
    {
        session_start();
        $response = $this->armOne->login([
            "Membershipkey" => $customer->getEmail(),
            "Password" => $customer->getPassword(),
            "Channel" => "ARM_PAYDAY",
            "RedirectURL" => $redirecturl
        ]);
        $_SESSION["token"];
        return $response;
    }

    /**
     *
     * @return type
     */
    public function validateARMOneCookie($token)
    {
        return $this->armOne->validateLoginSession([
                    "Channel" => "ARM_PAYDAY"
                        ],$token);
    }

    /**
     *
     * @param type $token
     * @return type
     */
    public function logout($token)
    {
        return $this->armOne->logout($token, [
                    "Channel" => "ARM_PAYDAY"
        ]);
    }

    /**
     *
     * @param array $data
     * @return type
     */
    public function register(array $data)
    {
        return $this->armOne->register($data);
    }

    /**
     *
     * @param Customer $customer
     * @return type
     */
    public function resetPassword(Customer $customer)
    {
        return $this->armOne->changePassword([
                    "Membershipkey" => $customer->getEmail(),
                    "OldPassword" => "",
                    "NewPassword" => $customer->getPassword(),
                    "IsReset" => true,
                    "Channel" => "ARM_PAYDAY"
        ]);
    }

}
