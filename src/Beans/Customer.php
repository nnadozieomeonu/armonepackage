<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NnadozieOmeonu\ARMOne\Beans;

/**
 * Description of Customer
 * This is a customers bean object that will be used to hold customer details
 * this bean will also be responsible for retriving any customer related personal information
 *
 * @author Victor_Omeonu
 */
class Customer
{

    //transfer object properties
    /**
     *
     * @var type
     */
    protected $surname;

    /**
     *
     * @var type
     */
    protected $firstname;

    /**
     *
     * @var type
     */
    protected $middlename;

    /**
     *
     * @var type
     */
    protected $username;

    /**
     *
     * @var type
     */
    protected $email;

    /**
     *
     * @var type
     */
    protected $phonenumber;

    /**
     *
     * @var type
     */
    protected $password;

    /**
     *
     * @var type
     */
    protected $securityQuestion;

    /**
     *
     * @var type
     */
    protected $securityAnswer;

    /**
     *
     * @var type
     */
    protected $maritalStatus;

    /**
     *
     * @var type
     */
    protected $gender;

    /**
     *
     * @var type
     */
    protected $state;

    /**
     *
     * @var type
     */
    protected $dob;

    /**
     *
     * @var type
     */
    protected $progressStatus;

    /**
     *
     * @var type
     */
    protected $profilePic;

    /**
     *
     * @var type
     */
    protected $bvn;

    /**
     *
     * @var type
     */
    protected $bankName;

    /**
     *
     * @var type
     */
    protected $bankAccountNo;

    /**
     *
     * @var type
     */
    protected $address;

    /**
     *
     * @var type
     */
    protected $registrationDate;

    /**
     *
     * @var type
     */
    protected $status;

    /**
     *
     * @var type
     */
    protected $userId;

    /**
     *
     * @var type
     */
    protected $prToken;

    /**
     *
     * @var type
     */
    protected $token;

    public function __construct()
    {

    }

    /**
     *
     * @return type
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     *
     * @return type
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     *
     * @return type
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     *
     * @return type
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @return type
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     *
     * @return type
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     *
     * @return type
     */
    public function getSecurityQuestion()
    {
        return $this->securityQuestion;
    }

    /**
     *
     * @return type
     */
    public function getSecurityAnswer()
    {
        return $this->securityAnswer;
    }

    /**
     *
     * @return type
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     *
     * @return type
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     *
     * @return type
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     *
     * @return type
     */
    public function getProgressStatus()
    {
        return $this->progressStatus;
    }

    /**
     *
     * @return type
     */
    public function getProfilePic()
    {
        return $this->profilePic;
    }

    public function getBvn()
    {
        return $this->bvn;
    }

    /**
     *
     * @return type
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     *
     * @return type
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     *
     * @return type
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     *
     * @return type
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     *
     * @return type
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @param type $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     *
     * @param type $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     *
     * @param type $middlename
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;
    }

    /**
     *
     * @param type $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     *
     * @param type $phonenumber
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }

    /**
     *
     * @param type $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     *
     * @param type $securityQuestion
     */
    public function setSecurityQuestion($securityQuestion)
    {
        $this->securityQuestion = $securityQuestion;
    }

    /**
     *
     * @param type $securityAnswer
     */
    public function setSecurityAnswer($securityAnswer)
    {
        $this->securityAnswer = $securityAnswer;
    }

    /**
     *
     * @param type $maritalStatus
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
    }

    /**
     *
     * @param type $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     *
     * @param type $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     *
     * @param type $progressStatus
     */
    public function setProgressStatus($progressStatus)
    {
        $this->progressStatus = $progressStatus;
    }

    /**
     *
     * @param type $profilePic
     */
    public function setProfilePic($profilePic)
    {
        $this->profilePic = $profilePic;
    }

    /**
     *
     * @param type $bvn
     */
    public function setBvn($bvn)
    {
        $this->bvn = $bvn;
    }

    /**
     *
     * @param type $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     *
     * @param type $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     *
     * @param type $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     *
     * @param type $registrationDate
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     *
     * @param type $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     *
     * @return type
     */
    function getUserId()
    {
        return $this->userId;
    }

    /**
     *
     * @param type $userId
     */
    function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     *
     * @return type
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     *
     * @param type $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     *
     * @return type
     */
    public function getPrToken()
    {
        return $this->prToken;
    }

    /**
     *
     * @return type
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *
     * @param type $prToken
     */
    public function setPrToken($prToken)
    {
        $this->prToken = $prToken;
    }

    /**
     *
     * @param type $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     *
     * @return type
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     *
     * @param type $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

}
