<?php

namespace NnadozieOmeonu\ARMOne\Services;

use NnadozieOmeonu\ARMOne\Contract\Requestable;
use GuzzleHttp\Client as Http;
use GuzzleHttp\Exception\ClientException;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
class ArmRequest implements Requestable
{

    /**
     *
     * @var type
     */
    private $armOneBaseUrl;

    /**
     *
     * @var type
     */
    private $http;

    /**
     *
     * @var type
     */
    private $token;

    /**
     *
     * @var type
     */
    private $request;

    public function __construct(Http $http)
    {
        $this->armOneBaseUrl = "http://stag-api.arm.com.ng/armauth/v1/ARMONE/";
        $this->http = $http;
        $this->token = "";
    }

    /**
     *
     * @return type
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *
     * @param type $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     *
     * @param type $method
     * @param type $endpoint
     * @param type $data
     * @return boolean
     * @throws ClientException
     *
     * {@inheritdoc}
     */
    public function handle($method, $endpoint, $data)
    {
        try {
            $r = $this->http->request($method, $this->armOneBaseUrl . $endpoint, [
                'body' => $data,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Token' => $this->getToken()
                ]
            ]);
            return json_decode($r->getBody(), true);
        } catch (ClientException $exception) {
            return false;
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            throw $exception;
        }
    }

}
