<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NnadozieOmeonu\ARMOne\Services;

use NnadozieOmeonu\ARMOne\Services\ArmRequest;
use GuzzleHttp\Client as Http;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
class ARMOneAuthenticationService
{

    /**
     *
     * @var type
     */
    private $request;

    //put your code here
    public function __construct()
    {
        $this->request = new ArmRequest(new Http());
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function register($data)
    {
        return $this->request->handle('POST', 'register', json_encode($data));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function login($data)
    {
        return $this->request->handle('POST', 'login', json_encode($data));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function changePassword($data)
    {
        return $this->request->handle('POST', 'ChangePassword', json_encode($data));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function validateLoginSession($data, $sessionKey)
    {
        $this->request->setToken($sessionKey);
        return $this->request->handle('POST', 'ValidateSession', json_encode($data));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function logout($sessionKey, $data)
    {
        $this->request->setToken($sessionKey);
        return $this->request->handle('POST', 'SignOut', json_encode($data));
    }

}
