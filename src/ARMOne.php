<?php

namespace NnadozieOmeonu\ARMOne;

use NnadozieOmeonu\ARMOne\Beans\Customer;
use NnadozieOmeonu\ARMOne\Contract\Impl\IdentityManagement;

/**
 * Class UserController
 * @package App\Http\Controllers
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
class ARMOne
{

    /**
     *
     * @var type
     */
    protected $customer;

    /**
     *
     * @var type
     */
    protected $identityManagement;

    public function __construct()
    {
        $this->customer = new Customer();
        $this->identityManagement = new IdentityManagement();
    }

    /**
     *
     * @param \NnadozieOmeonu\ARMOne\String $email
     * @param \NnadozieOmeonu\ARMOne\String $password
     * @return type
     */
    public function postLoginUser(String $email, String $password)
    {
        session_start();
        $this->customer->setEmail($email);
        $this->customer->setPassword($password);
        $response = $this->identityManagement->login($this->customer, "/validateARMOneSession");
        if ($response["ResponseCode"] == "00") {
            $_SESSION["email"] = $this->customer->getEmail();
            $_SESSION["token"] = $response["SessionKey"];
            header("Location: " . $response["RedirectURL"]);
        } else {
            return $response["ResponseCode"] . ": " . $response["StatusMessage"];
        }
    }

    /**
     *
     * @return type
     */
    public function validateSessionAndSetUserData()
    {
        session_start();
        $email = $_SESSION['email'];
        $token = "some token";
        $response = $this->identityManagement->validateARMOneCookie($token);
        return $response == "Authorized" && $this->clearApplicationData(['email']) ? true : false;
    }

    public function clearApplicationData(Array $datas)
    {
        session_start();
        foreach ($datas as $data) {
            unset($_SESSION[$data]);
        }
        return true;
    }

    /**
     *
     * @param \NnadozieOmeonu\ARMOne\String $newPassword
     * @return type
     */
    public function postForgotPassword(String $newPassword)
    {
        $this->customer->setPassword($newPassword);
        $response = $this->identityManagement->resetPassword($this->customer);
        return response != false and $response["ResponseCode"] == "00" ?
                true : false;
    }

    /**
     *
     * @param \NnadozieOmeonu\ARMOne\String $email
     * @param \NnadozieOmeonu\ARMOne\String $oldPasswordt
     * @param \NnadozieOmeonu\ARMOne\String $newPassword
     * @return type
     */
    public function postChangePassword(String $email, String $oldPasswordt, String $newPassword)
    {
        $response = $this->identityManagement->changePassword($email, $oldPasswordt, $newPassword);
        return response != false and $response["ResponseCode"] == "00" ?
                true : false;
    }

    /**
     *
     * @param Customer $client
     * @return type
     */
    public function postCreateUserAccount(Customer $client)
    {
        $this->customer = $client;
        $response = $this->identityManagement->register($client);
        return response != false and $response["ResponseCode"] == "00" ?
                true : false;
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function signOut($data)
    {
        $response = $this->identityManagement->logout();
        return ($response != false) ? true : false;
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function register($data)
    {
        $response = $this->identityManagement->register($data);
        return ($response != false) ? true : false;
    }

}
