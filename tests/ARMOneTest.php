<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use NnadozieOmeonu\ARMOne\Contract\Impl\IdentityManagement;
use NnadozieOmeonu\ARMOne\Beans\Customer;

/**
 * Description of IdentityManagement
 *
 * @author Victor_Omeonu
 */
class ARMOneTest extends PHPUnit_Framework_TestCase
{

    /**
     * PHP Unit test for register
     */
    public function testRegister()
    {
        $armOne = new IdentityManagement();
        /*
         * Implementation of ARM Register Endpoint
         *
         */
        print_r($armOne->register([
                    "Membershipkey" => 0,
                    "Password" => "password@17837",
                    "EmailAddress" => "email" . rand(10000, 9999999) . "@mailinator.com",
                    "MobileNumber" => "080" . rand(11111111, 99999999),
                    "SecurityQuestion" => "Test Question",
                    "SecurityQuestion2" => "NA",
                    "SecurityAnswer" => "Test Answer",
                    "SecurityAnswer2" => "NA",
                    "LastName" => "James",
                    "FirstName" => "Omeonu",
                    "Channel" => "ARM_PAYDAY"
        ]));
        /**
         * Testing that the request reached ARM One
         */
        $this->assertTrue(true);
    }

    /**
     * PHP Unit test for client login
     */
    public function testLogin()
    {
        $customer = new Customer();
        $customer->setEmail("a@abcd.com");
        $customer->setPassword("TestPassword@1234567");
        $armOne = new IdentityManagement();
        print_r(@$armOne->login($customer, "http://www.google.com"));
        $this->assertTrue(true);
    }

    /**
     * PHP Unit test for client logout
     */
    public function testLogout()
    {
        $armOne = new IdentityManagement();
        $token = "QQS8Z4T33Xae/Sc7Xi9UYCsKbta6kpvA5j24rzVJbEmWg6q/YHzFS5jiLlgzN2Wj";
        /*
         * Implementation of ARM Register Endpoint
         *
         */
        print_r($armOne->logout($token));
        $this->assertTrue(true);
    }

    /**
     * PHP Unit test to validate cookie
     */
    public function testValidateARMOneCookie()
    {
        $armOne = new IdentityManagement();
        $token = "QQS8Z4T33Xae/Sc7Xi9UYCsKbta6kpvA5j24rzVJbEmWg6q/YHzFS5jiLlgzN2Wj";
        /*
         * Implementation of ARM Register Endpoint
         *
         */
        print_r($armOne->validateARMOneCookie($token));
        $this->assertTrue(true);
    }

    /**
     * PHP Unit test to change password
     */
    public function testChangePassword()
    {
        $armOne = new IdentityManagement();
        $email = "a@abcd.com";
        $oldPassword = "TestPassword@1234567";
        $newPassword = "TestPassword@1234567-somenewpassword";
        /*
         * Implementation of ARM Register Endpoint
         *
         */
        print_r($armOne->changePassword($email, $oldPassword, $newPassword));
        $this->assertTrue(true);
    }

    /**
     * PHP Unit test to reset password
     */
    public function testResetPassword()
    {
        $armOne = new IdentityManagement();
        /*
         * Implementation of ARM Register Endpoint
         *
         */
        $customer = new Customer();
        $customer->setEmail("a@abcd.com");
        $customer->setPassword("TestPassword@1234567");
        print_r($armOne->resetPassword($customer));
        $this->assertTrue(true);
    }

}
